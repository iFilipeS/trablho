//
//  SecondViewController.swift
//  Istecbook4
//
//  Created by Jason on 22/12/2016.
//  Copyright © 2016 Something Corp. All rights reserved.
//

import UIKit
//passar tableView

//pedidos de amizade
var pamigos = ["Cristiano Ronaldo", "Tiger Woods", "Lindsey Stirling", "Kelly Slater", "Tony Hawks"]

//amigos em comum
var comum = ["5 amigos em comum", "2 amigos em comum", "10 amigos em comum", "7 amigos em comum", "25 amigos em comum"]

//fotos
var fotos = [UIImage(named: "cr"), UIImage(named: "tw"), UIImage(named: "ls"), UIImage(named: "ks"), UIImage(named: "th")]


class SecondViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CustomCellDelegate{

    @IBOutlet var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pamigos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! CustomCell
        
        cell.delegate = self
        
        cell.foto.image = fotos[indexPath.row]
        cell.name.text = pamigos[indexPath.row]
        cell.mutual.text = comum[indexPath.row]
        
        return cell
    }
    
    func reloadTableData() {
        tableView.reloadData()
    }
    
    //muda para viewcontroller amigos
    @IBAction func cpedidosamigos(_ sender: AnyObject) {
        performSegue(withIdentifier: "pedidosamigos", sender: self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

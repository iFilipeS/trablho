//
//  ViewController.swift
//  Istecbook4
//
//  Created by Jason on 22/12/2016.
//  Copyright © 2016 Something Corp. All rights reserved.
//

/*
 */

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var textfield: UITextField!
    //labels
    @IBOutlet weak var Told: UILabel!
    @IBOutlet weak var Post: UILabel!
    
    @IBOutlet weak var told1: UILabel!
    @IBOutlet weak var post1: UILabel!
    
    @IBOutlet weak var Told3: UILabel!
    @IBOutlet weak var Post3: UILabel!
    
    var timer = 30
    var ttimer = Timer()
    var disse = " disse:"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textfield.delegate = self
        
        self.Told.text = ""
        self.Post.text = ""
        
        self.told1.text = ""
        self.post1.text = ""
        
        self.Told3.text = ""
        self.Post3.text = ""
        
        //Exemplo de Timer
        timer = 30
        ttimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ViewController.finish), userInfo: nil, repeats: true)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func cfeedpedidos(_ sender: AnyObject) {
        performSegue(withIdentifier: "feedpedidos", sender: self)
    }
    
    @IBAction func cfeedamigos(_ sender: AnyObject) {
        performSegue(withIdentifier: "feedamigos", sender: self)
    }
    
    //Feed
    //publica
    @IBAction func pubica(_ sender: Any) {
        let tfpost = String(textfield.text!)
        let ntold = "Joaquim disse:"
        
        if (tfpost?.isEmpty)!{
        }else{
            Post3.text = String(post1.text!)
            Told3.text = String(told1.text!)
            
            post1.text = String(Post.text!)
            told1.text = String(Told.text!)
            
            Post.text = tfpost
            Told.text = ntold
            UserDefaults.standard.set(Post3.text, forKey: "P3")
            UserDefaults.standard.set(Told3.text, forKey: "T3")

            UserDefaults.standard.set(post1.text, forKey: "P1")
            UserDefaults.standard.set(told1.text, forKey: "T1")

            UserDefaults.standard.set(Post.text, forKey: "P")
            UserDefaults.standard.set(Told.text, forKey: "T")

            
        }
        self.textfield?.text = nil;
    }
    
    //apaga o texto da label em "feed"
    @IBAction func cleartext(_ sender: AnyObject) {
        self.textfield?.text = nil;
    }
    
    //post automaticos
    func stimer(_ sender: AnyObject){
    }
    func finish (){
        timer -= 1
        UserDefaults.standard.set(timer, forKey: "time")
        if timer == 15 {
            Post3.text = String(post1.text!)
            Told3.text = String(told1.text!)
            
            post1.text = String(Post.text!)
            told1.text = String(Told.text!)
            
            Post.text = "Se esperares algum tempo vai aparecer post automáticos. #BUILDAWALL"
            Told.text = amigos[0] + disse
            UserDefaults.standard.set(Post3.text, forKey: "P3")
            UserDefaults.standard.set(Told3.text, forKey: "T3")
            
            UserDefaults.standard.set(post1.text, forKey: "P1")
            UserDefaults.standard.set(told1.text, forKey: "T1")
            
            UserDefaults.standard.set(Post.text, forKey: "P")
            UserDefaults.standard.set(Told.text, forKey: "T")
            
        }
        if timer == 5{
            Post3.text = String(post1.text!)
            Told3.text = String(told1.text!)
            
            post1.text = String(Post.text!)
            told1.text = String(Told.text!)
            
            Post.text = "Boa esperaste apareceu um post!! Se esperares mais, vão aparecer mais"
            Told.text = amigos[1] + disse
            UserDefaults.standard.set(Post3.text, forKey: "P3")
            UserDefaults.standard.set(Told3.text, forKey: "T3")
            
            UserDefaults.standard.set(post1.text, forKey: "P1")
            UserDefaults.standard.set(told1.text, forKey: "T1")
            
            UserDefaults.standard.set(Post.text, forKey: "P")
            UserDefaults.standard.set(Told.text, forKey: "T")
        }
        if timer == -15{
            Post3.text = String(post1.text!)
            Told3.text = String(told1.text!)
            
            post1.text = String(Post.text!)
            told1.text = String(Told.text!)
            
            Post.text = "I am going to voice Dory. Where am I?"
            Told.text = amigos[4] + disse
            UserDefaults.standard.set(Post3.text, forKey: "P3")
            UserDefaults.standard.set(Told3.text, forKey: "T3")
            
            UserDefaults.standard.set(post1.text, forKey: "P1")
            UserDefaults.standard.set(told1.text, forKey: "T1")
            
            UserDefaults.standard.set(Post.text, forKey: "P")
            UserDefaults.standard.set(Told.text, forKey: "T")
        }
        if timer == -35{
            Post3.text = String(post1.text!)
            Told3.text = String(told1.text!)
            
            post1.text = String(Post.text!)
            told1.text = String(Told.text!)
            
            Post.text = "I still can't believe I am President of the USA!"
            Told.text = amigos[0] + disse
            UserDefaults.standard.set(Post3.text, forKey: "P3")
            UserDefaults.standard.set(Told3.text, forKey: "T3")
            
            UserDefaults.standard.set(post1.text, forKey: "P1")
            UserDefaults.standard.set(told1.text, forKey: "T1")
            
            UserDefaults.standard.set(Post.text, forKey: "P")
            UserDefaults.standard.set(Told.text, forKey: "T")
        }
        if timer == -40{
            Post3.text = String(post1.text!)
            Told3.text = String(told1.text!)
            
            post1.text = String(Post.text!)
            told1.text = String(Told.text!)
            
            Post.text = "OOOHHH Finalmente meti mais 1 vídeo no Youtubee"
            Told.text = amigos[2] + disse
            UserDefaults.standard.set(Post3.text, forKey: "P3")
            UserDefaults.standard.set(Told3.text, forKey: "T3")
            
            UserDefaults.standard.set(post1.text, forKey: "P1")
            UserDefaults.standard.set(told1.text, forKey: "T1")
            
            UserDefaults.standard.set(Post.text, forKey: "P")
            UserDefaults.standard.set(Told.text, forKey: "T")
        }
        if timer == -50{
            if amigos.count == 6{
                Post3.text = String(post1.text!)
                Told3.text = String(told1.text!)
            
                post1.text = String(Post.text!)
                told1.text = String(Told.text!)
            
                Post.text = "ahhh é Natal"
                Told.text = amigos[5] + disse
                UserDefaults.standard.set(Post3.text, forKey: "P3")
                UserDefaults.standard.set(Told3.text, forKey: "T3")
                
                UserDefaults.standard.set(post1.text, forKey: "P1")
                UserDefaults.standard.set(told1.text, forKey: "T1")
                
                UserDefaults.standard.set(Post.text, forKey: "P")
                UserDefaults.standard.set(Told.text, forKey: "T")
            }else{
                Post3.text = String(post1.text!)
                Told3.text = String(told1.text!)
                
                post1.text = String(Post.text!)
                told1.text = String(Told.text!)
                
                Post.text = "ahhh é Natal"
                Told.text = amigos[3] + disse
                UserDefaults.standard.set(Post3.text, forKey: "P3")
                UserDefaults.standard.set(Told3.text, forKey: "T3")
                
                UserDefaults.standard.set(post1.text, forKey: "P1")
                UserDefaults.standard.set(told1.text, forKey: "T1")
                
                UserDefaults.standard.set(Post.text, forKey: "P")
                UserDefaults.standard.set(Told.text, forKey: "T")
            }
        }
        if timer == -70{
            if amigos.count == 9{
                Post3.text = String(post1.text!)
                Told3.text = String(told1.text!)
                
                post1.text = String(Post.text!)
                told1.text = String(Told.text!)
                
                Post.text = "Estamos quase no fim dos post automaticos"
                Told.text = amigos[8] + disse
                UserDefaults.standard.set(Post3.text, forKey: "P3")
                UserDefaults.standard.set(Told3.text, forKey: "T3")
                
                UserDefaults.standard.set(post1.text, forKey: "P1")
                UserDefaults.standard.set(told1.text, forKey: "T1")
                
                UserDefaults.standard.set(Post.text, forKey: "P")
                UserDefaults.standard.set(Told.text, forKey: "T")
            }else{
                Post3.text = String(post1.text!)
                Told3.text = String(told1.text!)
                
                post1.text = String(Post.text!)
                told1.text = String(Told.text!)
                
                Post.text = "Se nao tivesses 9 amigos não era eu que postava"
                Told.text = amigos[3] + disse
                UserDefaults.standard.set(Post3.text, forKey: "P3")
                UserDefaults.standard.set(Told3.text, forKey: "T3")
                
                UserDefaults.standard.set(post1.text, forKey: "P1")
                UserDefaults.standard.set(told1.text, forKey: "T1")
                
                UserDefaults.standard.set(Post.text, forKey: "P")
                UserDefaults.standard.set(Told.text, forKey: "T")
            }
        }
        if timer == -75{
            if amigos.count == 7{
                Post3.text = String(post1.text!)
                Told3.text = String(told1.text!)
                
                post1.text = String(Post.text!)
                told1.text = String(Told.text!)
                
                Post.text = "Falta 1 dia para o NATAL!!!"
                Told.text = amigos[6] + disse
                UserDefaults.standard.set(Post3.text, forKey: "P3")
                UserDefaults.standard.set(Told3.text, forKey: "T3")
                
                UserDefaults.standard.set(post1.text, forKey: "P1")
                UserDefaults.standard.set(told1.text, forKey: "T1")
                
                UserDefaults.standard.set(Post.text, forKey: "P")
                UserDefaults.standard.set(Told.text, forKey: "T")
            }else{
                Post3.text = String(post1.text!)
                Told3.text = String(told1.text!)
                
                post1.text = String(Post.text!)
                told1.text = String(Told.text!)
                
                Post.text = "Nunca mais comeca 2017"
                Told.text = amigos[3] + disse
                UserDefaults.standard.set(Post3.text, forKey: "P3")
                UserDefaults.standard.set(Told3.text, forKey: "T3")
                
                UserDefaults.standard.set(post1.text, forKey: "P1")
                UserDefaults.standard.set(told1.text, forKey: "T1")
                
                UserDefaults.standard.set(Post.text, forKey: "P")
                UserDefaults.standard.set(Told.text, forKey: "T")
            }
        }
        if timer == -90{
            if amigos.count == 7{
                Post3.text = String(post1.text!)
                Told3.text = String(told1.text!)
                
                post1.text = String(Post.text!)
                told1.text = String(Told.text!)
                
                Post.text = "Ultimo post automático"
                Told.text = amigos[6] + disse
                UserDefaults.standard.set(Post3.text, forKey: "P3")
                UserDefaults.standard.set(Told3.text, forKey: "T3")
                
                UserDefaults.standard.set(post1.text, forKey: "P1")
                UserDefaults.standard.set(told1.text, forKey: "T1")
                
                UserDefaults.standard.set(Post.text, forKey: "P")
                UserDefaults.standard.set(Told.text, forKey: "T")
            }else{
                Post3.text = String(post1.text!)
                Told3.text = String(told1.text!)
                
                post1.text = String(Post.text!)
                told1.text = String(Told.text!)
                
                Post.text = "Este foi o último post automático"
                Told.text = amigos[3] + disse
                UserDefaults.standard.set(Post3.text, forKey: "P3")
                UserDefaults.standard.set(Told3.text, forKey: "T3")
                
                UserDefaults.standard.set(post1.text, forKey: "P1")
                UserDefaults.standard.set(told1.text, forKey: "T1")
                
                UserDefaults.standard.set(Post.text, forKey: "P")
                UserDefaults.standard.set(Told.text, forKey: "T")
            }
        }
    }
    //Esconder keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        }
    //Esconder
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textfield.resignFirstResponder()
        return(true)
    }
    //guardar
    override func viewDidAppear(_ animated: Bool) {
        //guardar post
        if let x = UserDefaults.standard.object(forKey: "P3")as? String
        {
            Post3.text = x
        }
        if let y = UserDefaults.standard.object(forKey: "T3")as? String
        {
            Told3.text = y
        }
        
        if let z = UserDefaults.standard.object(forKey: "P1")as? String
        {
            post1.text = z
        }
        if let a = UserDefaults.standard.object(forKey: "T1")as? String
        {
            told1.text = a
        }
        
        if let b = UserDefaults.standard.object(forKey: "P")as? String
        {
            Post.text = b
        }
        if let c = UserDefaults.standard.object(forKey: "T")as? String
        {
            Told.text = c
        }
        //guardar timer
        if let d = UserDefaults.standard.object(forKey: "time")as? Int
        {
            timer = d
        }
    }
}

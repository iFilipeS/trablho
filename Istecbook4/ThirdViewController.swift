//
//  ThirdViewController.swift
//  Istecbook4
//
//  Created by Jason on 22/12/2016.
//  Copyright © 2016 Something Corp. All rights reserved.
//

import UIKit

//amigos
var amigos = ["Donald Trump","Jennifer Lawrence", "Markiplier", "Oprah Winfrey", "Ellen Degeneres"]

//fotos
var fotos2 = [UIImage(named: "dt"), UIImage(named: "jl"), UIImage(named: "mp"), UIImage(named: "ow"), UIImage(named: "ed")]

class ThirdViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var tableView2: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return amigos.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell2 = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! CustomCell2
        
        cell2.foto2.image = fotos2[indexPath.row]
        cell2.name2.text = amigos[indexPath.row]
        
        return cell2
    }

    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

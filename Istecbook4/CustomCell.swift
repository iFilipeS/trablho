//
//  CustomCell.swift
//  Istecbook4
//
//  Created by Jason on 22/12/2016.
//  Copyright © 2016 Something Corp. All rights reserved.
//

import UIKit

protocol CustomCellDelegate {
    func reloadTableData()
}

class CustomCell: UITableViewCell{
    
    var delegate: CustomCellDelegate?
    
    @IBOutlet var foto: UIImageView!
    @IBOutlet var name: UILabel!
    @IBOutlet var mutual: UILabel!
    
    
    @IBAction func aceitar(_ sender: Any) {
        amigos.insert(String(pamigos[0]), at: 5)
        fotos2.insert(fotos[0], at: 5)
        pamigos.removeFirst()
        fotos.removeFirst()
        
        delegate?.reloadTableData()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}

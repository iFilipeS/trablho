//
//  CustomCell2.swift
//  Istecbook4
//
//  Created by Jason on 22/12/2016.
//  Copyright © 2016 Something Corp. All rights reserved.
//

import UIKit

class CustomCell2: UITableViewCell {

    @IBOutlet var foto2: UIImageView!
    @IBOutlet var name2: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
